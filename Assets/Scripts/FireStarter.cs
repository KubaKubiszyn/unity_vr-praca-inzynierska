﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireStarter : MonoBehaviour
{
    Transform fireSystem;
    bool isOnfire = false;
    [SerializeField]float temperature = 10f;
    // Start is called before the first frame update
    void Start()
    {
        fireSystem = this.transform.Find("Fire_Particle2");
        fireSystem.gameObject.SetActive(false);
        isOnfire = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (temperature < 2000f)
        {
            if (temperature < 500)
            {
                temperature += 1;
                fireSystem.gameObject.SetActive(true);
                SetFlameSize(1f);
                fireSystem.GetComponent<SphereCollider>().radius += 0.005f;

            }
            else if (temperature >= 500 && temperature < 1200)
            {
                temperature += 1.5f;
                SetFlameSize(3f);
                fireSystem.GetComponent<SphereCollider>().radius += 0.005f;

            }
            else if (temperature >= 1200)
            {
                SetFlameSize(5f);
                temperature += 2.5f;
                fireSystem.GetComponent<SphereCollider>().radius += 0.005f;

            }
        }


    }
    public void SetFlameSize(float size)
    {
        ParticleSystem particle = transform.Find("Fire_Particle2").GetComponent<ParticleSystem>();
        var main = particle.main;
        main.startSize = size;
    }
}
