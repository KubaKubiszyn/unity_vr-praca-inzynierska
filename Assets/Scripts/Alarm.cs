﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : MonoBehaviour
{

    AudioSource audioSource;
    [SerializeField] AudioClip alarmSound;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();        
    }

    // Update is called once per frame
    public void PlayAlarmSound()
    {
        audioSource.PlayOneShot(alarmSound);
    }
}
