﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glass : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] AudioSource aS;
    [SerializeField] AudioClip[] aC;
    [SerializeField] GameObject glassShater;
    byte strength = 2;
    


    void Start()
    {
        aS = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShatterGlass()
    {
        if (strength == 2)
        {
            aS.PlayOneShot(aC[0]);
            glassShater.SetActive(true);
            strength--;
        }
        else if (strength == 1)
        {
            AudioSource.PlayClipAtPoint(aC[1],this.transform.position);
            Destroy(this.gameObject);
            strength--;

        }

        


    }

}
