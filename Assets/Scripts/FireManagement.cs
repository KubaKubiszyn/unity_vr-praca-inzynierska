﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireManagement : MonoBehaviour
{
    [SerializeField] List<Transform> Rooms;
    float fireSpreadInRoom = 1.5f;
    float fireSPreadBetweenRooms = 4f;

    public void StartFire()
    {
        IEnumerator fireStarter = ManageFireInBuilding(Rooms);
        StartCoroutine(fireStarter);
    }
    IEnumerator ManageFireInRoom(Transform room)
    {
        foreach (Transform fire in room)
        {
            fire.gameObject.SetActive(true);
            yield return new WaitForSeconds(fireSpreadInRoom);
        }
    }

    IEnumerator ManageFireInBuilding(List<Transform> fireArray)
    {
        foreach (var room in fireArray)
        {
            IEnumerator corout = ManageFireInRoom(room);
            StartCoroutine(corout);
            yield return new WaitForSeconds(fireSPreadBetweenRooms);
        }
        


    }
}
