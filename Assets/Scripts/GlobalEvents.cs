﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalEvents : MonoBehaviour
{
    #region SerializedFields
    [SerializeField] List<Transform> lights;
    [SerializeField] float smokeEmitersDelay = 5f;
    [SerializeField] float stageDelay = 10f;

    #endregion

    #region References
    ShaderModifier shaderModifier;
    Transform smokeEmiters;
    #endregion

    #region Variables

   [SerializeField] public bool fireStarted = false;
   [SerializeField] public bool stage1 = false;
   [SerializeField] public bool stage2 = false;

   [SerializeField] private float stage0Timer = 0f;
   [SerializeField] private float stage1Timer = 0f;
   [SerializeField] private float stage2Timer = 0f;



    #endregion

    #region Start()
    void Start()
    {
        shaderModifier = GetComponent<ShaderModifier>();
        smokeEmiters = this.transform.Find("SmokeEmitters");
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }


    } 
    #endregion

    #region Update()
    void Update()
    {
        ManageStages();       
    }

    private void ManageStages()
    {

        if (!stage1)
        {
            stage0Timer += Time.deltaTime;
        }

        if (stage1 && !stage2)
        {
            stage1Timer += Time.deltaTime;
        }

        if (stage2 && stage2Timer < stageDelay)
        {
            stage2Timer += Time.deltaTime;
        }

        if (stage0Timer > stageDelay && !stage1)
        {
            stage1 = true;
            FindObjectOfType<Alarm>().PlayAlarmSound();
        }

        if (stage1Timer > stageDelay && !stage2)
        {
            GetComponent<FireManagement>().StartFire();
            StartCoroutine("EnableSmokeEmiters");
            Blackout();
            stage2 = true;
        }

    }

    #endregion

    #region SmokeEmitters
    IEnumerator EnableSmokeEmiters()
    {
        smokeEmiters.gameObject.SetActive(true);
        foreach (Transform child in smokeEmiters)
        {
            child.gameObject.SetActive(true);
            yield return new WaitForSeconds(smokeEmitersDelay);
        }
    }
    #endregion

    #region Blackout
    public void Blackout()
    {
        foreach (var light in lights)
        {
            light.gameObject.SetActive(false);
        }
        shaderModifier.RendererBlackout();

    }
    #endregion

}
