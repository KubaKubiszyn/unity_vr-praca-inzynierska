﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MonoBehaviour
{
    [SerializeField] float playersHealth;
    [SerializeField] float damageCooldown = 1f;
    float lastDamaged = 0f;
    public bool canBeDamaged = true;

    private void Update()
    {
        if (lastDamaged < damageCooldown)
        {
            lastDamaged += Time.deltaTime;
        }
        if(lastDamaged >= damageCooldown && !canBeDamaged)
        {
            canBeDamaged = true;
        }

    }
    public void DamagePlayer(int damage)
    {
        playersHealth -= damage;
        print("player damaged");
        canBeDamaged = false;
        lastDamaged = 0f;
    }

}
