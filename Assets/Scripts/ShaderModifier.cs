﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderModifier : MonoBehaviour
{
    [SerializeField] List<Transform> LightsMaterial;

    private void Start()
    {
    }
    // Update is called once per frame
    public void RendererBlackout()
    {
        foreach (var light in LightsMaterial)
        {            light.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");

        }
           
    }
}
