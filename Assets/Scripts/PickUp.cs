﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PickUp : MonoBehaviour
{
    AudioSource audiosrce;
    [SerializeField] AudioClip impactClip;


  
    [SerializeField] Transform theDest = null;
    bool canBeThrown = false;
    bool isBeeingHeld = false;
    float dropCooldown = 1f;


    private void Start()
    {
        audiosrce = GetComponent<AudioSource>();
    }


    private void Update()
    {
        if (dropCooldown > 0) dropCooldown -= Time.deltaTime;
        if (isBeeingHeld)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }
    public void PickItem()
    {
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().freezeRotation = true;
       

        this.transform.position = theDest.position;
        this.transform.parent = GameObject.Find("Destination").transform;
        canBeThrown = true;
        isBeeingHeld = true;


    }

    public void DropItem()
    {
        this.transform.parent = null;
        GetComponent<Rigidbody>().freezeRotation = false;
        GetComponent<Rigidbody>().useGravity = true;
        canBeThrown = false;
        isBeeingHeld = false;
 
    }

    public void ThrowItem(float throwStrength, Vector3 direction)
    {
        if(canBeThrown)
        {
            GetComponent<Rigidbody>().AddForce(direction * throwStrength, ForceMode.Impulse);
            this.transform.parent = null;
            GetComponent<Rigidbody>().freezeRotation = false;
            GetComponent<Rigidbody>().useGravity = true;
            canBeThrown = false;
            isBeeingHeld = false;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Walls" && dropCooldown <= 0f && !isBeeingHeld)
        {
            audiosrce.PlayOneShot(impactClip);
            dropCooldown = 1f;
        }

        if (collision.transform.tag == "Glass" && dropCooldown <= 0f && !isBeeingHeld)
        {
            collision.transform.GetComponent<Glass>().ShatterGlass();
            dropCooldown = 1f;
        }
           
            

    }
}
