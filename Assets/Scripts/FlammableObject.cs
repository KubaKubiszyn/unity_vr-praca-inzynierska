﻿using UnityEngine;

public class FlammableObject : MonoBehaviour
{

    Transform fireSystem;
    [SerializeField] float temperature = 10;
    // Start is called before the first frame update
    void Start()
    {
        fireSystem = this.transform.Find("Fire_Particle2");
        fireSystem.gameObject.SetActive(false);
        fireSystem.GetComponent<FireSpread>().Active=false;
        

    }

    // Update is called once per frame
    void Update()
    {
        
   
        
    }

    public void IncreaseTemp(float amount)
    {
        if (temperature <= 2000)
        {
            if (temperature < 500)
            {
                temperature += amount;
                IncreaseRadius(0.005f);

            }
            else if (temperature >= 500 && temperature < 1200)
            {

                temperature += amount;
                SetFlameSize(3f);
                IncreaseRadius(0.005f);

                if(fireSystem.gameObject.activeSelf == false)
                {
                    fireSystem.gameObject.SetActive(true);
                }

                if (fireSystem.GetComponent<FireSpread>().Active == false)
                {
                    fireSystem.GetComponent<FireSpread>().Active = true;
                }
                
               
                    
                

            }
            else if (temperature >= 1200)
            {
                SetFlameSize(5f);
                temperature += amount;
                IncreaseRadius(0.005f);

            }
        }
        
    }

    private void IncreaseRadius(float amount)
    {
        if (fireSystem.GetComponent<SphereCollider>().radius <= 10 && fireSystem.gameObject.activeSelf == true)
        {
            fireSystem.GetComponent<SphereCollider>().radius += amount;
        }
            
    }

    public void SetFlameSize(float size)
    {
    
            ParticleSystem particle = transform.Find("Fire_Particle2").GetComponent<ParticleSystem>();
            var main = particle.main;
            main.startSize = size;


    }
 
}
