﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;


public class RotateCamera : MonoBehaviour
{

    [SerializeField] float speedH = 4.0f;
    [SerializeField] float speedV = 4.0f;

    [SerializeField] float yaw = 0.0f;
    [SerializeField] float pitch = 0.0f;

    float ThrowSpeed = 10f;
    PickUp currentPickup;
    bool isCarryingSomething = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        yaw += speedH * CrossPlatformInputManager.GetAxis("Mouse X");
        pitch -= speedV * CrossPlatformInputManager.GetAxis("Mouse Y");
        
        transform.eulerAngles = new Vector3 (Mathf.Clamp(pitch,-70,70),yaw,0.0f);
        HandleItems();


    }

    void HandleItems()
    {
        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                if (hit.transform.GetComponent<PickUp>() != null && Vector3.Distance(this.transform.position, hit.transform.position) <= 3f)
                {
                 
                    currentPickup = hit.transform.GetComponent<PickUp>();
                    currentPickup.PickItem();
                    isCarryingSomething = true;
                }


            }

           
         
        }

        if (CrossPlatformInputManager.GetButtonUp("Fire2") && isCarryingSomething == true)
        {
            currentPickup.ThrowItem(ThrowSpeed, transform.forward);
            isCarryingSomething = false;

        }

        if (CrossPlatformInputManager.GetButtonUp("Fire1"))
        {
            currentPickup.DropItem();
            isCarryingSomething = false;

        }



    }
}
