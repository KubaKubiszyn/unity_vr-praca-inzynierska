﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] float playerSpeed = 10f;
    [SerializeField] float jumpCooldown = 1f;
    [SerializeField] AudioClip[] walkingSounds;
    AudioSource playerAudioSource;
    float jumpForce = 4f;
    float jumpTimer = 0f;
    [SerializeField] float stepSoundCooldown = 1;
    float stepSoundTimer = 0;
    bool inAir = false;
    // Start is called before the first frame update
    void Start()
    {
        playerAudioSource = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
    }
    void MovePlayer()
    {
        //TODO SPRINT

        if (CrossPlatformInputManager.GetAxis("Vertical") != 0 && CrossPlatformInputManager.GetAxis("Horizontal") != 0)
        { playerSpeed = 7f; }
        else { playerSpeed = 10f; }

        
        transform.position += new Vector3(
         Camera.main.transform.forward.x,
         0, 
         Camera.main.transform.forward.z) 
         * playerSpeed * Time.deltaTime * CrossPlatformInputManager.GetAxis("Vertical")
        + new Vector3(
            Camera.main.transform.right.x,
            0,
            Camera.main.transform.right.z)
         * playerSpeed * Time.deltaTime * CrossPlatformInputManager.GetAxis("Horizontal");
        
        jumpTimer -= Time.deltaTime;
        if(jumpTimer<=0)
        {
            inAir = false;
        }

        if (CrossPlatformInputManager.GetButtonDown("Jump") && jumpTimer <= 0)
        {
            inAir = true;
            jumpTimer = jumpCooldown;
            GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce, ForceMode.Impulse);

        }

        stepSoundTimer -= Time.deltaTime;

        if ((CrossPlatformInputManager.GetAxis("Vertical") != 0 || CrossPlatformInputManager.GetAxis("Horizontal") != 0) && stepSoundTimer <= 0 && !inAir)
        {
            playerAudioSource.Stop();
            playerAudioSource.PlayOneShot(walkingSounds[Random.Range(1, 3)]);
            stepSoundTimer = stepSoundCooldown;
        }


        if (CrossPlatformInputManager.GetButton("Fire2"))
        {
            GetComponent<CapsuleCollider>().height = 1.5f;

        }
        else
        {
            GetComponent<CapsuleCollider>().height = 3f;

        }

    }

 
}
