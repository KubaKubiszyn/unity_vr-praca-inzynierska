﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSpread : MonoBehaviour
{
    float tempWsp;
    private bool isActive = false;
    public bool Active { 
        get
    {
        return isActive;
        
    } 
        set
    {
        isActive = value;
    } 

    }
    // Start is called before the first frame update

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Flammable") 
        {
            
            if (other.transform.GetComponent<FlammableObject>() != null)
            {

               FlammableObject fObject = other.transform.GetComponent<FlammableObject>();
                float dist = Vector3.Distance(this.transform.position, other.transform.position);
                fObject.IncreaseTemp(2.5f * 1/dist);
            }
        }
        
    }
}
