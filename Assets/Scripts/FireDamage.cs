﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireDamage : MonoBehaviour
{
    CapsuleCollider collider;
    PlayerStatus player;
    [SerializeField] float baseDamage;
    [SerializeField] float damageModifier;
    void Start()
    {
        collider = GetComponent<CapsuleCollider>();
        player = GameObject.Find("Player").GetComponent<PlayerStatus>();
    }

    private void Update()
    {
      
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player" && player.canBeDamaged == true)
        {
            float distance = Vector3.Distance(other.transform.position, this.transform.position);
            other.GetComponent<PlayerStatus>().DamagePlayer(10);
        }
            
    }
}
